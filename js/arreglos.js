console.log("arreglos");

// Arrays (multiple types)
var elements = ["Cata", 18, "Cardozo"];
console.log(elements);

// Access an element by index
var element = elements[0]; // Read
console.log(element);
elements[1] = 20; // Update
console.log(elements);
// Append data at the end of array: push
elements.push(200);
console.log(elements);
// Remove last element: pop
elements.pop();
console.log(elements);
// Remove first element: shift
elements.shift();
console.log(elements);
// Add element at start: unshift
elements.unshift("Medellin");
console.log(elements);
// Copy of an array
var elements2 = [...elements];
elements2.push("Colombia");
console.log(elements2);
console.log(elements);

var matrix = [
  ["A", "B", "C"],
  ["D", "E", "F"],
  ["G", "H", "I"],
];

console.log(matrix[0][1]);
matrix.push(["J", "K", "L"]);
console.log(matrix);

//
let arreglo = [2, 5, 1, 6];
let res = arreglo.join("=");
console.log(res);
res = arreglo.slice(1, 3); // seccion del arreglo
console.log(res);

res = arreglo.reduce((x, y) => x + y); // Sumatoria
console.log(res);

res = arreglo.map((e) => e * 2);
console.log(res);

res = arreglo.sort();
console.log(res);

arreglo.forEach((x) => console.log(x));

arreglo.forEach((x) => {
  console.log(x);
  console.log(x + 1);
});

let numeros = [1, 2, 3, 4, 9];
res = numeros.every((x) => x % 2 == 0); // TODOS
console.log(res);
res = numeros.some((x) => x % 2 == 0); // AL MENOS UNO
console.log(res);
res = numeros.filter((x) => x % 2 == 0); // filtrar
console.log(res);