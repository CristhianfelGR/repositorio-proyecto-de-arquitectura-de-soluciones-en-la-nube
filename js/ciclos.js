console.log('ciclos');
// WHILE
var x = 0;
while (x < 5) {
    console.log(x); 
    x++;
}
// FOR
for (let x = 0; x < 5; x++) {
    console.log(x);
}
// FOR ARREGLO
let arreglo = [10, 20, 30, 40]

let i = 0;
while (i < arreglo.length) {
    console.log(arreglo[i]);
    i++;
}

for (let x = 0; x < arreglo.length; x++) {
    console.log(arreglo[x]);
}

arreglo.forEach(x => console.log(x));