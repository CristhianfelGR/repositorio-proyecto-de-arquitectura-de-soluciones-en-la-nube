// Definición de la clase
class Persona {
    constructor(nombre, edad) {
      this._nombre = nombre;
      this._edad = edad;
    }
    set nombre(nombre) {
      this._nombre = nombre;
    }
    get nombre() {
      return this._nombre;
    }
  }
  
  // Creación del objeto
  let p = new Persona("Catalina", 18);
  p.nombre = "Maria";
  console.log(p.nombre);
  
  var musica = [
    {
      id: 1,
      cancion: "One",
      artista: "Metallica",
      formato: { nombre: "CD", year: 1983 },
    },
    { id: 2, cancion: "Hello", artista: "Adele" },
    { id: 3, cancion: "Stars", artista: "Simply Red" },
  ];
  
  console.log(musica[0].formato.year); // notacion .
  console.log(musica[0]["formato"]["year"]); // notacion []
  
  // Otro for
  for (let cancion of musica) {
    console.log(cancion);
  }
  
  let pixel = {
    x: 4,
    y: 7,
  };
  // Clasica
  let x = pixel.x;
  let y = pixel.y;
  console.log(x, y);
  
  // Destructuring
  const { x: valorX, y: valorY } = pixel;
  console.log(valorX, valorY);
  
  const CLIMA = {
    hoy: { min: 15, max: 20 },
    manana: { min: 16, max: 19 },
  };
  // destructuring de objetos compuestos
  const {
    hoy: { min: u },
  } = CLIMA;
  console.log(u);
  // destructuring de arreglos
  const [variableI, , , variableJ] = [10, 20, 30, 40, 50];
  console.log(variableI);
  console.log(variableJ);
  
  let m = 3
  let x = 4
  // swap clasico
  let temp = m;
  m = x;
  x = temp;
  // swap destructurixg
  [m, x] = [x, m]
  
  var user = {
      nombre: "john",
      edad: 20,
      ciudad: "bogota",
      pais: "colombia",
      hobbies: ["videojuegos", "cine", "taekwondo"]
  }
  
  function imprimir({ciudad, pais}){
      console.log(ciudad, pais)
  }
  
  imprimir(user)