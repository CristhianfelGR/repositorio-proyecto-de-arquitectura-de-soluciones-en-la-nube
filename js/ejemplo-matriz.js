// Lista de emojis:
// https://unicode.org/emoji/charts/full-emoji-list.html
var valorBase = 0x1f613;
const a = String.fromCodePoint(valorBase);
const b = String.fromCodePoint(valorBase + 1);
const c = String.fromCodePoint(valorBase + 2);
const d = String.fromCodePoint(valorBase + 3);
const e = String.fromCodePoint(valorBase + 4);
const f = String.fromCodePoint(valorBase + 5);
const g = String.fromCodePoint(valorBase + 6);
const h = String.fromCodePoint(valorBase + 7);
const i = String.fromCodePoint(valorBase + 8);
var matriz = [
  [a, b, c],
  [d, e, f],
  [g, h, i],
];
var mensaje = "";
// Recorre las filas
for (let fila = 0; fila < matriz.length; fila++) {
  // Recorre los elementos en la fila
  for (let elemento = 0; elemento < matriz[fila].length; elemento++) {
    // Agrega el elemento a un string
    mensaje += matriz[fila][elemento] + "\t";
  }
  mensaje += "\n";
}
// Muestra el string resultante
console.log(mensaje);

// Recorrido de matriz con forEach
mensaje = "";
matriz.forEach((fila) => {
  fila.forEach((elem) => {
    mensaje += elem + "\t";
  });
  mensaje += "\n";
});
console.log(mensaje);
