// FUNCTIONS

// Define function
function myFunction() {
    console.log("This is my function!");
  }
  // Call the function
  myFunction();
  
  // Function with arguments
  function sum(num1, num2){
      return num1 + num2;
  }
  // Call function with arguments
  var result = sum(4, 3);
  console.log(result);
  
  // OPTIONAL PARAMETERS
  function incrementValue(value, increment=1){
      return value + increment;
  }
  
  console.log(incrementValue(5, 2));
  console.log(incrementValue(10));