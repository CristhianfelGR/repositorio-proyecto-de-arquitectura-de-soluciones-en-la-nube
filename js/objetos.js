let usuario = {
    nombre: "Catalina",
    edad: 18,
    altura: 1.78,
    esAdulto: true,
    notas: [1, 2, 3],
    ubicacion: {
      ciudad: "Bogota",
      pais: "Colombia",
    },
    canciones: [
      { nombre: "Help", artista: "The Beatles" },
      { nombre: "One", artista: "Metallica" },
      { nombre: "Stars", artista: "Simply Red" },
    ],
    cantidadNotas() {
      return this.notas.length;
    },
  };
  //console.log(usuario);
  console.log(usuario.nombre);
  console.log(usuario["nombre"]);
  
  usuario.nombre = "Maria"; // modificando una propiedad
  usuario["edad"] = 19; // modificando una propiedad
  
  console.log(usuario.notas);
  console.log(usuario.cantidadNotas());
  
  usuario.apellido = "Cardozo";
  var existe = usuario.hasOwnProperty("apellido")
  console.log(existe);
  
  delete usuario.apellido;    // elimina una propiedad
  existe = usuario.hasOwnProperty("apellido")
  console.log(existe);
  
  // var, let, const
  const ciudad = {
    nombre: "Bogota",
    pais: "Colombia"
  }
  Object.freeze(ciudad);
  // No modifica pero no genera error
  ciudad.nombre = "Medellin"  
  console.log(ciudad)