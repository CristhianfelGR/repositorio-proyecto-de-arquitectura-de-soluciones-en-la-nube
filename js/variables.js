console.log("Hello from another file 2!!!");
/*
Data types:
undefined, null, number, string, boolean, objects
*/
// Scope = all program
var myName = "John";
console.log(myName);
myName = "Alexander";
console.log(myName);

// Scope = in the block
let myCity = "Medellin";
console.log(myCity);

// Constantes
const PI = 3.1416;
console.log(PI);
//PI = 4.5; // Raise error

var a, b, c;

a = 3; // assign a constant
b = a; // assign a variable
c = a + b; // assign a expression
console.log(a, b, c);

var sum = 10 + 10; // add
sum++; //increment
sum += 4; // increment operator +=
console.log(sum);

var subs = 10 - 10;
subs--;
subs -= 3;
console.log(subs);

var mult = 2 * 3;
mult *= 2;
console.log("Multiplicacion =", mult);

var divide = 10.5 / 3.7;
divide /= 2;
console.log(divide);

var remainder = 20 % 6;
console.log(remainder);